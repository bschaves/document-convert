#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from __future__ import annotations
import re
import pandas
from pandas import DataFrame
from typing import List, Dict
from io import BytesIO

from libconvert import *
from libconvert.common import get_path_tesseract_system

def main():
    OUT = Directory('/home/brunoc/Downloads/TESTE')
    OUT.mkdir()
    out = OUT.join_file('exemplo.txt')
    pdf = '/home/brunoc/Documentos/AOO/exame final - 6p tcc.pdf'
    
    pages = PageDocumentPdf.from_file_pdf(pdf)
    pages[0].to_file_txt(out.absolute())
    
    
    
main()