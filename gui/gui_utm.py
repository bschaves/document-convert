#!/usr/bin/env python3
#

import os
import time
import pandas
import numpy
from typing import (List)
import threading
from threading import Thread, Event
from pandas import DataFrame
import pandas

from libconvert import (
    File,
    Directory,
    FilesTypes,
    SheetInputStream,
    export_dataframe,
    ParseDF,
    ConvertPoints,
    UtmPoint,
    LatLongPoint,
)

import tkinter as tk
from tkinter import ttk

from gui import (
    show_warnnings,
    AppPage,
)

# Gerar converter coordenadas
class PageConvertToLatLon(AppPage):
    def __init__(self, *, parent, controller):
        super().__init__(parent=parent, controller=controller)
        self.pageName = '/home/select_actions/to_latlon'
        self.frame_master = self.widgets.get_frame(self)
        self.frame_master.pack()
        #
        self.__parse: ParseDF = None
        self._current_progress:float = 0
        self.initUI()

    def initUI(self):
        #================================================================#
        # CONVETER DE PONTO
        #================================================================#
        self.containerPointUtm = ttk.Frame(self.frame_master, style='LightPurple.TFrame')
        self.containerPointUtm.pack(expand=True, fill='both', padx=2, pady=2)
        # Label no topo
        self.labelConvertFromPoint = self.widgets.get_label(self.containerPointUtm)
        self.labelConvertFromPoint.config(text='Converter Ponto UTM para Latitude e Longitude')
        self.labelConvertFromPoint.pack(padx=1, pady=1)
        
        #================================================================#
        # Container para entrada de texto UTM Leste
        #================================================================#
        self.frameUtmEast = ttk.Frame(self.containerPointUtm)
        self.frameUtmEast.pack(expand=True, fill='both', padx=2, pady=1)
        
        self.labelTextUtmEast = self.widgets.get_label(self.frameUtmEast)
        self.labelTextUtmEast.config(text='UTM Leste:')
        self.labelTextUtmEast.pack(side=tk.LEFT, expand=True, fill='both', padx=2, pady=1)

        self.input_text_easting = ttk.Entry(self.frameUtmEast)
        self.input_text_easting.pack(expand=True, fill='both', padx=2, pady=1)
        
        self.labelTextUtmNort = self.widgets.get_label(self.containerPointUtm)
        self.labelTextUtmNort.config(text='UTM Norte:')
        self.labelTextUtmNort.pack(side=tk.LEFT, expand=True, fill='both', padx=2, pady=1)

        self.input_text_northing = ttk.Entry(self.containerPointUtm)
        self.input_text_northing.pack(expand=True, fill='both', padx=2, pady=1)
        
        #================================================================#
        # Frame Result
        #================================================================#
        self.containerResult = ttk.Frame(self.frame_master)
        self.containerResult.pack()
        
        # Label do resultado da conversão
        self.label_result = ttk.Label(self.containerResult)
        
        # Botão copiar
        self.btn_copy = self.widgets.get_button(self.containerResult)
        self.btn_copy.config(
            text='Copiar', 
            command=self.copy_lat_lon,
        )
        
        #================================================#
        # Container INFORMAÇÕES DA PLANILHA
        #================================================#
        self.containerComboSheet = ttk.Frame(self.frame_master, style='DarkPurple.TFrame')
        self.containerComboSheet.pack(expand=True, fill='both', padx=1, pady=1)
        
        # Texto Informativo no topo
        self._containerTopInfo = ttk.Frame(self.containerComboSheet)
        self._containerTopInfo.pack(expand=True, fill='both', padx=2, pady=2)
        #
        self.label_top_info_sheets = self.widgets.get_label(self._containerTopInfo)
        self.label_top_info_sheets.config(text='Informações da planilha')
        self.label_top_info_sheets.pack(padx=1, pady=1)

        # Frame Sheet Nort
        self.frame_sheet_nort = self.widgets.get_frame(self.containerComboSheet)
        self.frame_sheet_nort.pack(expand=True, fill='both', padx=1, pady=1)
        # Label Sheet Nort
        self.label_sheet_nort = self.widgets.get_label(self.frame_sheet_nort)
        self.label_sheet_nort.config(text='Coluna UTM Norte: ')
        self.label_sheet_nort.pack(side=tk.LEFT, padx=2, pady=2)
        # Coluna zone North
        self.columns_sheet = []
        self.combo_column_utm_nort = self.widgets.get_combobox(
            parent=self.frame_sheet_nort,
            values=self.columns_sheet,
        )
        self.combo_column_utm_nort.pack()
        self.combo_column_utm_nort.set('-')

        # Frame Sheet East
        self.frame_sheet_east = self.widgets.get_frame(self.containerComboSheet)
        self.frame_sheet_east.pack(expand=True, fill='both', padx=1, pady=1)
        # Label Sheet East
        self.label_sheet_east = self.widgets.get_label(self.frame_sheet_east)
        self.label_sheet_east.config(text='Coluna UTM Oeste: ')
        self.label_sheet_east.pack(side=tk.LEFT, padx=1, pady=1)

        # Coluna zone East
        self.columns_sheet = []
        self.combo_column_utm_east = self.widgets.get_combobox(
            parent=self.frame_sheet_east,
            values=self.columns_sheet,
        )
        self.combo_column_utm_east.set('-')
        self.combo_column_utm_east.pack()
        
        # Selecionar uma planilha do disco
        self.btnSelectSheet = ttk.Button(
            self.containerComboSheet, 
            text='Selecionar planilha', 
            command=self._select_file_sheet,
        )
        self.btnSelectSheet.pack(expand=True, fill='both', padx=1, pady=1)
        
        self.labelSelectedSheet = ttk.Label(self.containerComboSheet, text='Nenhuma planilha selecionada.')
        self.labelSelectedSheet.pack(expand=True, fill='both', padx=1, pady=1)
        
        #================================================================#
        # Container para informações da zona
        #================================================================#
        self.containerZone = ttk.Frame(self.frame_master, style='DarkOrange.TFrame')
        self.containerZone.pack(expand=True, fill='both', padx=3, pady=3)
        
        # Label no topo do container
        self._containerTextZoneInfo = ttk.Frame(self.containerZone)
        self._containerTextZoneInfo.pack(expand=True, fill='both', padx=2, pady=2)
        #
        self.labelTextZone = self.widgets.get_label(self._containerTextZoneInfo)
        self.labelTextZone.config(text='Informações da Zona')
        self.labelTextZone.pack(padx=1, pady=1)
        
        #================================================================#
        # Container Zone Num
        #================================================================#
        self.containerZoneNum = self.widgets.get_frame(self.containerZone)
        self.containerZoneNum.pack(expand=True, fill='both', padx=2, pady=1)
        
        self.label_zone_num = self.widgets.get_label(self.containerZoneNum)
        self.label_zone_num.config(text='Número da Zona:')
        self.label_zone_num.pack(side=tk.LEFT, expand=True, fill='both', padx=1, pady=1)
        
        self.combo_zone_num = self.widgets.get_combobox(
            parent=self.containerZoneNum,
            values=[x for x in range(1, 60)],
        )
        self.combo_zone_num.pack(side=tk.LEFT, expand=True, fill='both', padx=1, pady=1)
        self.combo_zone_num.set('20')
        
        #================================================================#
        # Container Zone Letter
        #================================================================#
        self.containerZoneLetter = self.widgets.get_frame(self.containerZone)
        self.containerZoneLetter.pack(expand=True, fill='both', padx=1, pady=1)
        
        self.label_letter_zone = self.widgets.get_label(self.containerZoneLetter)
        self.label_letter_zone.config(text='Letra da Zona:')
        self.label_letter_zone.pack(side=tk.LEFT, expand=True, fill='both', padx=1, pady=1)
        
        # Combo Zone Letter
        self.values_letter = [
            'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
            'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
        ]
        self.combo_zone_letter = self.widgets.get_combobox(
            parent=self.containerZoneLetter,
            values=self.values_letter,
        )
        self.combo_zone_letter.pack(side=tk.LEFT, expand=True, fill='both', padx=1, pady=1)
        self.combo_zone_letter.set('K')

        #================================================#
        # Container com radio buttons para selecionar o tipo
        # de conversão, de ponto ou de planilha.
        #================================================#
        # Frame Radios
        self.containerRadios = ttk.Frame(self.frame_master,)
        self.containerRadios.config(relief='groove')
        self.containerRadios.pack(expand=True, fill='both', padx=2, pady=3)    
        
        # Label informativo
        self._containerLabelSelect = ttk.Frame(self.containerRadios)
        self._containerLabelSelect.pack(expand=True, fill='both', padx=3, pady=2)
        #
        self.labelTypeSelect = ttk.Label(
            self._containerLabelSelect, 
            text='Tipo de seleção (planilha ou ponto UTM)'
        )
        self.labelTypeSelect.pack(padx=1, pady=1)
        
        # Container para as opções
        self._containerOptionsRadio = ttk.Frame(self.containerRadios)
        self._containerOptionsRadio.pack(expand=True, fill='both', padx=2, pady=2)

        # Radio para selecionar o tipo de conversão
        self.radio_value_convert:tk.StringVar = tk.StringVar(value='from_point')

        # De ponto
        self.radio_from_point = self.widgets.get_radio_button(self._containerOptionsRadio)
        self.radio_from_point.config(
                        text='De Ponto', 
                        variable=self.radio_value_convert,
                        value='from_point',
                    )
        self.radio_from_point.pack(side=tk.LEFT, expand=True, fill='both', padx=2, pady=1)

        # De arquivo
        self.radio_from_file = self.widgets.get_radio_button(self._containerOptionsRadio)
        self.radio_from_file.config(
                        text='De Arquivo', 
                        variable=self.radio_value_convert,
                        value='from_file',
                    )
        self.radio_from_file.pack(side=tk.LEFT, expand=True, fill='both', padx=2, pady=1)
        
        # Botão converter
        self.btnConvert = ttk.Button(self.containerRadios, text='Converter', command=self.run_convert)
        self.btnConvert.pack(expand=True, fill='both', padx=2, pady=1)
        
        #================================================#
        # container barra de progresso.
        #================================================#
        self.containerProgressBar = self.widgets.get_frame(self.frame_master)
        self.containerProgressBar.pack(expand=True, fill='both', padx=2, pady=2)
        self.progressBar = self.widgets.container_pbar(self.containerProgressBar)
    
    def setParse(self, p:ParseDF):
        if not isinstance(p, ParseDF):
            return
        self.__parse = p
        self.update_comboboxs()
        
    def getParse(self) -> ParseDF:
        return self.__parse

    def copy_lat_lon(self):
        """
            Exportar uma coordenada convertida para área de transferência.
        """
        self.parent.clipboard_clear()
        _text = self.label_result['text']
        self.parent.clipboard_append(_text)
        self.parent.update()

    def run_convert(self):
        """Executar a conversão em uma thread para exibir a barra de progresso"""
        e = threading.Thread(target=self.__execute_convert)
        e.start()

    def __execute_convert(self):
        if not self.check_running():
            return
        
        self.start_pbar()
        if self.radio_value_convert.get() =='from_file':
            self._convert_from_sheet()
        elif self.radio_value_convert.get() =='from_point':
            _current_east = self.input_text_easting.get()
            _current_nort = self.input_text_northing.get()
            try:
                _utm = UtmPoint(
                    east=_current_east[0:6] if len(_current_east) > 6 else _current_east,
                    north=_current_nort[0:7] if len(_current_nort) > 7 else _current_nort,
                    zone=self.combo_zone_num.get(),
                    letter=self.combo_zone_letter.get(),
                )
            except Exception as e:
                print(e)
                self.label_result.config(text='Erro tente novamente!')
                self.label_result.pack()
            else:
                try:
                    lat_long = _utm.to_latlong()
                    if lat_long is None:
                        self.label_result.config(text='Erro tente novamente!')
                        self.label_result.pack()
                    else:
                        self.label_result.config(text=lat_long.to_string())
                        self.label_result.pack()
                        self.btn_copy.pack()
                        self.copy_lat_lon()
                except:
                    pass
        self.stop_pbar()

    def _convert_from_sheet(self):
        """Converter as coordenadas apartir de uma planilha excel"""
        # Selecionar um arquivo para salvar os dados.
        output:str = self.controller.openFiles.save_file(FilesTypes.EXCEL)
        if (not output) or (output is None):
            show_warnnings('Nenhuma arquivo foi salvo!')
            return
        output_file = File(output)
        
        if not self.getParse().exists_columns(
                [self.combo_column_utm_east.get(), self.combo_column_utm_nort.get()]
            ):
            show_warnnings('Colunas inválidas, tente novamente!')
            return
        
        # Converter
        self.start_pbar()
        self.set_progress('-')
        self.set_text_pbar('Convertendo UTM em Latitude Longitude')
        convert:ConvertPoints = ConvertPoints()
        df = convert.from_data_utm(
            data=self.getParse().data,
            column_east=self.combo_column_utm_east.get(),
            column_north=self.combo_column_utm_nort.get(),
            zone=int(self.combo_zone_num.get()),
            letter=self.combo_zone_letter.get(),
        )
        
        self.set_text_pbar(f'Exportando arquivo: {output_file.basename()}')
        export_dataframe(df, output_file)
        self.set_text_pbar(f'OK')
        self.stop_pbar()
    
    def _select_file_sheet(self):
        """
            Caixa de dialogo para selecionar uma planilha
        """
        if not self.check_running():
            return
        
        self.controller.select_file(FilesTypes.EXCEL)
        if self.controller.numSelectedFiles < 1:
            return
        
        self.labelSelectedSheet.config(text=f'Planilha: {self.controller.selectedUserFiles[0].basename()}')
        self.btnSelectSheet.config(text='Alterar')
        self.load_data()
    
    def load_data(self):
        th = threading.Thread(target=self.__execute_load_data)
        th.start()
    
    def __execute_load_data(self):
        """
            Ler os valores da planilha selecionada
        """
        if self.controller.numSelectedFiles < 1:
            show_warnnings('Selecione uma planilha com coordenadas UTM para prosseguir!')
            return
        
        self.start_pbar()
        self.set_text_pbar(f'Lendo a planilha:\n{self.controller.selectedUserFiles[0].basename()}')
        input_stream = SheetInputStream(self.controller.selectedUserFiles[0], load_now=True)
        while True:
            if not input_stream.is_running():
                break
            self.set_progress(f'{input_stream.get_progress():.2f}')
            time.sleep(1)
            
        try:
            self.setParse(ParseDF(input_stream.get_data()))
        except Exception as e:
            print(f'{__class__.__name__}\n{e}')
        
        self.set_text_pbar('OK')
        self.set_progress('100')
        self.stop_pbar()
        
    def update_comboboxs(self) -> None:
        if (self.getParse() is None) or (self.getParse().data.empty):
            show_warnnings('Dados vazios tente novamente!')
            return
        
        self.combo_column_utm_east['values'] = self.getParse().data.columns.tolist()
        self.combo_column_utm_nort['values'] = self.getParse().data.columns.tolist()
        self.combo_column_utm_east.set(self.getParse().data.columns.tolist()[0])
        self.combo_column_utm_nort.set(self.getParse().data.columns.tolist()[0])

    def set_text_pbar(self, text):
        if text is None:
            return
        self.progressBar.set_text_pbar(text)
        
    def set_progress(self, text):
        self.progressBar.set_text_progress(text)

    def start_pbar(self):
        self.running = True
        self.progressBar.start_pbar()

    def stop_pbar(self):
        self.progressBar.stop_pbar()
        self.running = False

    def set_size_screen(self):
        self.parent.title('Converter UTM para Latitude e Longitude')
        self.parent.geometry('450x490')

    def update_state(self):
        pass
