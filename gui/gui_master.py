#!/usr/bin/env python3
#
from __future__ import annotations
import os
from typing import (List, Callable)
from threading import Thread

import tkinter as tk
from tkinter import (
        Tk,
        Menu,  
        filedialog, 
        messagebox,
)

from gui.gui_version import (
    __author__,
    __version__,
    __url__,
    __update__,
    __version_lib__,
)

from gui import (
    show_warnnings,
    GetWidgets,
    AppPage,
    ControllerApp,
    Navigator,
)

from gui.gui_pdf import (
    PageConvertPdfs,
)

from gui.gui_images import (
    PageImagesToPdf,
)

from gui.gui_sheets import (
    PageFilesToExcel,
    PageMoveFiles,
    PageDocsToi,
    PageSoupSheets,
)

from gui.gui_recognize import (
    PageRecognizeImages,
    PageRecognizePDF,
)

from gui.gui_utm import PageConvertToLatLon

from libconvert.common import (
    UserAppDir,
    get_path_tesseract_system,
)

from libconvert import (
    JsonConvert,
    JsonData,
    File,
)


#========================================================#
# Nova Interface
#========================================================#
class PageSelectActions(AppPage):
    def __init__(self, *, parent, controller):
        super().__init__(parent=parent, controller=controller)
        self.pageName = '/home'
        self.widgets = GetWidgets(self)
        self.frame_master = self.widgets.get_frame(self)
        self.frame_master.pack(padx=20, pady=20)
        self.frame_master.config(style='Custom.TFrame')
        self.padding=(8, 9)

        self.initUI()
        
    def initUI(self):
        self._start_widgets()
        # Botão voltar
        self.btn_back = self.widgets.get_button(self.frame_master)
        self.btn_back.config(text='Voltar', command=lambda: self.controller.navigatorPages.pop())
        self.btn_back.pack()
      
    def _start_widgets(self):
        #=========================================================#
        # Frame para botões de reconhecimento de texto OCR
        #=========================================================#
        self.container_buttons_ocr = self.widgets.get_frame(self.frame_master)
        self.container_buttons_ocr.config(style="LightPurple.TFrame")
        self.container_buttons_ocr.pack(expand=True, fill='both', padx=1, pady=1)
        
        # Texto de topo
        self.label_text_ocr_docs = self.widgets.get_label(self.container_buttons_ocr, text='OCR')
        self.label_text_ocr_docs.pack(padx=1, pady=1)
        
        # Container OCR 1
        self.container_ocr1 = self.widgets.get_frame(self.container_buttons_ocr)
        self.container_ocr1.pack(expand=True, fill='both', padx=3, pady=3)
        
        # Botão Reconhecer Imagens
        self.btn_images_to_pdf = self.widgets.get_button(
            self.container_ocr1, 
            text='Reconhecer Imagens (OCR)',
            cmd=lambda: self.controller.navigatorPages.push('/home/select_actions/page_ocr_imgs')
        )
        self.btn_images_to_pdf.pack(side=tk.LEFT, expand=True, fill='both', padx=1, pady=1)
        
        # Botão Reconhecer PDF
        self.btn_recognize_pdfs = self.widgets.get_button(
            self.container_ocr1,
            text='Reconhecer PDFs (OCR)',
            cmd=lambda: self.controller.navigatorPages.push('/home/select_actions/page_ocr_pdf'),
        )
        self.btn_recognize_pdfs.pack(side=tk.LEFT, expand=True, fill='both', padx=1, pady=1)

        #=========================================================#
        # Frame para botões PDF
        #=========================================================#
        self.frame_buttons_pdf = self.widgets.get_frame(self.frame_master)
        self.frame_buttons_pdf.pack(expand=True, fill='both', padx=2, pady=2)
        # Texto de topo do Frame
        self.label_info_pdf = self.widgets.get_label(self.frame_buttons_pdf)
        self.label_info_pdf.config(text='Editar PDF')
        self.label_info_pdf.pack()

        # Primeiro container PDF
        self.container_pdf1 = self.widgets.get_frame(self.frame_buttons_pdf)
        self.container_pdf1.config()
        self.container_pdf1.pack(expand=True, fill='both', padx=2, pady=2)

        # Converter PDF
        self.btn_uniq_pdf = self.widgets.get_button(self.container_pdf1)
        self.btn_uniq_pdf.config(
            text='Converter PDF',
            command=lambda: self.controller.navigatorPages.push('/home/select_actions/convert_pdf'),
        )
        self.btn_uniq_pdf.pack(side=tk.LEFT, expand=True, fill='both')

        #=========================================================#
        # Frame para botões Imagens
        #=========================================================#
        self.contanier_imgs1 = self.widgets.get_frame(self.frame_master)
        self.contanier_imgs1.pack(expand=True, fill='both', padx=2, pady=2)
        # Label
        self.label_info_img = self.widgets.get_label(self.contanier_imgs1)
        self.label_info_img.config(text='Editar Imagens')
        self.label_info_img.pack()
    
        # Imagens Para PDF
        self.btn_imgs_to_pdf = self.widgets.get_button(self.contanier_imgs1)
        self.btn_imgs_to_pdf.config(
            text='Imagens para PDF', 
            command=lambda: self.controller.navigatorPages.push('/home/select_actions/imgs_to_pdf'),
        )
        self.btn_imgs_to_pdf.pack(side=tk.LEFT, expand=True, fill='both')

        #=========================================================#
        # Frame para botões Coordenadas.
        #=========================================================#
        self.frame_coordinates = self.widgets.get_frame(self.frame_master)
        self.frame_coordinates.pack(padx=3, pady=3, fill='both', expand=True,)
        
        # Label de topo
        self.label_top_coordinates = self.widgets.get_label(self.frame_coordinates)
        self.label_top_coordinates.config(text='Coordenadas')
        self.label_top_coordinates.pack()
        
        # Botão LatLon para UTM
        self.btn_latlon_to_utm = self.widgets.get_button(self.frame_coordinates)
        self.btn_latlon_to_utm.config(
            text='Latitude Longitude para UTM',
            padding=self.padding,
            command=(),
        )
        self.btn_latlon_to_utm.pack(padx=1, pady=3, fill='both', expand=True, side=tk.LEFT)   
        
        # Botão UTM para LatLon
        self.btn_utm_to_latlon = self.widgets.get_button(self.frame_coordinates)
        self.btn_utm_to_latlon.config(
                text='UTM para Latitude Longitude',
                padding=self.padding,
                command=lambda: self.controller.navigatorPages.push('/home/select_actions/to_latlon'),
            )
        self.btn_utm_to_latlon.pack(padx=1, pady=3, fill='both', expand=True, side=tk.LEFT)

        #=========================================================#
        # Frame para botões Excel
        #=========================================================#
        self.frame_buttons_excel = self.widgets.get_frame(self.frame_master)
        self.frame_buttons_excel.pack(padx=4, pady=4, fill='both', expand=True)
        
        # Label de topo do Frame
        self.label_text_info_excel = self.widgets.get_label(self.frame_buttons_excel)
        self.label_text_info_excel.config(text='Utilitários de Planilhas')
        self.label_text_info_excel.pack()
        
        # 1 - Botão planilhar pasta
        self.btn_folder_to_sheet = self.widgets.get_button(self.frame_buttons_excel)
        self.btn_folder_to_sheet.config(
                        text='Planilhar Pasta', 
                        padding=self.padding,
                        command=lambda: self.controller.navigatorPages.push('/home/select_actions/folder_to_excel')
                    )
        self.btn_folder_to_sheet.pack(side=tk.LEFT, fill='both', expand=True)
        
        # 2 - Mover arquivos
        self.btn_move_files = self.widgets.get_button(self.frame_buttons_excel)
        self.btn_move_files.config(
            text='Mover arquivos',
            padding=self.padding,
            command=lambda: self.controller.navigatorPages.push('/home/select_actions/page_mv_files'),
        )
        self.btn_move_files.pack(side=tk.LEFT, fill='both', expand=True)
        
        # 3 - Botão filtrar dados em Excel/CSV
        self.btn_filter_sheets = self.widgets.get_button(self.frame_buttons_excel)
        self.btn_filter_sheets.config(
                text='Filtrar Planilhas', 
                command=self.go_page_soup_sheets,
            )
        self.btn_filter_sheets.pack(side=tk.LEFT, fill='both', expand=True)
        
        # 4 - Botão TOI
        self.btn_toi_to_sheet = self.widgets.get_button(self.frame_buttons_excel)
        self.btn_toi_to_sheet.config(
                text='Especial', 
                command=lambda: self.controller.navigatorPages.push('/home/select_actions/page_toi'),
        )
        self.btn_toi_to_sheet.pack(side=tk.LEFT, fill='both', expand=True)
        
        #=========================================================#
        # Frame para botões de teste
        #=========================================================#
        self.frame_buttons_teste = self.widgets.get_frame(self.frame_master)
        self.frame_buttons_teste.pack(padx=4, pady=4, fill='both', expand=True)
        
                
                
    def go_page_toi(self):
        self.controller.to_page('/home/select_actions/page_toi')

    def go_page_soup_sheets(self):
        self.controller.navigatorPages.push('/home/select_actions/soup_sheets')
    
    def set_size_screen(self):
        self.parent.geometry("510x400")
        self.parent.title(f"OCR Tool - Selecione uma ação")


class HomePage(PageSelectActions):
    def __init__(self, *, parent, controller):
        super().__init__(parent=parent, controller=controller)

        #self.pageName = '/home'
        
    def initUI(self):
        pass

    def go_page_select_actions(self):
        #self.controller.navigatorPages.push('/home/select_actions')
        pass        


class MyApplication(ControllerApp):
    def __init__(self, *, appDirs:UserAppDir):
        super().__init__(appDirs=appDirs)

        self.root:Tk = self
        self.widgets:GetWidgets = GetWidgets(self)
        self.initUI()

        # Gerenciar páginas
        self.controller:ControllerApp = self
        self.navigatorPages: Navigator = Navigator(parent=self, controller=self.controller)

        # Páginas
        _pages = (
            HomePage,
            PageSelectActions,

            PageRecognizeImages,
            PageRecognizePDF,
            PageImagesToPdf,

            PageDocsToi,
            PageFilesToExcel,
            PageMoveFiles,
            
            PageSoupSheets,
            PageConvertPdfs,
            
            PageConvertToLatLon,
            
        )
        for page in _pages:
            self.navigatorPages.add_page(page)

        self.pages = self.navigatorPages.pages
        self.last_frame = self.pages['/home']
        self.navigatorPages.push('/home')
    
    def initUI(self):
        self.title("OCR Tool")
        self.geometry("460x460")
        self.initMenuBar()

    def initMenuBar(self) -> None:
        # Variáveis para armazenar os caminhos dos arquivos
        # Tesseract
        
        # Criar o menu principal
        self.menu_bar = Menu(self.root)
        self.root.config(menu=self.menu_bar)

        self.create_menu_file()
        self.create_menu_config()
        self.create_menu_about()

    def create_menu_file(self):
        # Criar o menu Arquivo
        self.menu_file = Menu(self.menu_bar, tearoff=0)
        self.menu_bar.add_cascade(label="Arquivo", menu=self.menu_file)

        if os.path.exists(self.user_prefs.prefs['path_tesseract']):
            file_tesseract = self.user_prefs.prefs['path_tesseract']
        else:
            file_tesseract = get_path_tesseract_system().absolute()

        # Adicionar itens ao menu Arquivo
        self.tesseract_index = self.add_item_menu_file(
            label="Tesseract",
            tooltip=file_tesseract,
            command=lambda: self.select_bin_file_tesseract("tesseract"),
        )
        
        #
        self.cmd_go_back_page = self.add_item_menu_file(
            label='Voltar',
            tooltip='Voltar para a página anterior',
            command=lambda: self.navigatorPages.pop(),
        )

        self.exit_cmd = self.add_item_menu_file(
            label='Sair', 
            tooltip='Sair do programa', 
            command=self.exit_app
        )

    def add_item_menu_file(self, label: str, tooltip: str, command: Callable[[], None]) -> int:
        """
        Adiciona um item ao menu 'Arquivo' com um tooltip.

        :param label: Nome do item no menu.
        :param tooltip: Texto do tooltip exibido no menu.
        :param command: Função a ser chamada ao clicar no item.
        :return: Índice do item adicionado no menu.
        """
        self.menu_file.add_command(
            label=f"{label} ({tooltip})",
            command=command,
        )
        return self.menu_file.index(tk.END)
    
    def create_menu_config(self):
        self.menu_config = Menu(self.menu_bar, tearoff=0)
        self.menu_bar.add_cascade(label="Configurações", menu=self.menu_config)
        
        # Incluir itens no menu configurações
        self.menu_config.add_command(
            label=f"Arquivo de configuração: -",
            command=self.change_file_config,
        )
        self.menu_config.index(tk.END)

    def select_bin_file_tesseract(self, label: str) -> None:
        """
        Abre um diálogo para selecionar um arquivo e armazenar o caminho em uma variável.

        :param label: Rótulo do item no menu.
        """
        path_file: str = filedialog.askopenfilename(
            title=f"Selecione um arquivo para {label}",
            initialdir=self.openFiles.history_dirs.initialDir,
        )
        if path_file:
            # Armazena o caminho do arquivo selecionado na variável correspondente
            if label == "tesseract":
                self.user_prefs.prefs['path_tesseract'] = path_file
                self.menu_file.entryconfig(self.tesseract_index, label=f"Tesseract: {path_file}")
                self.user_prefs.prefs['path_tesseract'] = path_file
            elif label == "ocrmypdf":
                pass
            messagebox.showinfo(label, f"Caminho selecionado:\n{path_file}")
        else:
            messagebox.showinfo(label, "Nenhum arquivo foi selecionado.")

    def create_menu_about(self) -> None:
        """Exibe informações sobre o programa.""" 
        self.autor = f'Autor: {__author__}'
        self.versao = f'Versão: {__version__} | Atualização {__update__}' 
        self.version_lib = f'Lib Convert: {__version_lib__}'
        self.url = f'URL: {__url__}'

        self.menu_sobre = Menu(self.menu_bar, tearoff=0)
        self.menu_bar.add_cascade(label="Sobre", menu=self.menu_sobre)

        self.add_items_menu_about()
        self.root.config(menu=self.menu_bar)
    
    def add_items_menu_about(self):
        self.menu_sobre.add_command(label=self.autor,)
        self.menu_sobre.add_command(label=self.versao,)
        self.menu_sobre.add_command(label=self.version_lib)
        self.menu_sobre.add_command(label=self.url,)

    def change_file_config(self) -> None:
        """
            Alterar o arquivo de configuração
        """
        filename:str = filedialog.askopenfilename(
            title=f"Selecione um arquivo para JSON",
            initialdir=self.controller.openFiles.history_dirs.initialDir,
            filetypes=[("Arquivos JSON", "*.json")]
        )
        if not filename:
            return
        if not os.path.isfile(filename):
            return
        
        self.controller.fileConfigJson = File(filename)
        self.update_state()

    def update_menu_bar(self):
        self.menu_config.entryconfig(
            0,
            label=f'Arquivo: {self.controller.fileConfigJson.absolute()}'
        )
        self.controller.user_prefs.prefs['last_inputdir'] = self.controller.fileConfigJson.dirname()
        self.controller.user_prefs.prefs['file_json'] = self.controller.fileConfigJson.absolute()
        self.controller.user_prefs.prefs = JsonConvert().from_file(self.controller.fileConfigJson).to_dict()
        print(self.controller.user_prefs.prefs)

    def update_state(self):
        self.update_menu_bar()

    def exit_app(self):
        # Salvar as configurações alteradas, antes de sair
        print(f'Salvando configurações em: [{self.controller.fileConfigJson.absolute()}]')
        data:JsonData = JsonConvert().from_dict(self.controller.user_prefs.prefs)
        data.to_file(self.controller.fileConfigJson)
        print('OK')
        self.root.quit()


# Criação da janela principal e execução da interface do aplicativo
def runApp():
    app = MyApplication(appDirs=UserAppDir('Document-Convert'))
    app.mainloop()

if __name__ == "__main__":
    runApp()

