#!/usr/bin/env python3
#
from libconvert import version
__version__ = '0.2.2'
__author__ = 'Bruno Chaves'
__url__ = '-'
__update__ = '2025-02-23'
__version_lib__ = version.__version_lib__
